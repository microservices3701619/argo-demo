FROM maven:3.8.3-openjdk-17 AS builder
COPY . /app
WORKDIR /app
RUN mvn clean package

FROM openjdk:17-jdk-slim
WORKDIR /app
COPY --from=builder /app/target/courses-api-0.0.1.jar  .
CMD ["java", "-jar", "courses-api-0.0.1.jar", "--spring.profiles.active=remote"]

